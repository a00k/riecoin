Optimized RIC miner, embedded in riecoind by a00k
-----------------------------------------------

* Linux only (using gcc intrinsics, and /proc/meminf)
* v0.0.1 refined the stability issue of sieve size probing. You 
can also manually set the sieve size.
* Use newer verion of GCC(>=4.6), otherwise we may see SIGILL.
* You may change some parameters hard-codeded here in main.cpp. 

        class PSieve 
        {
        public:
            static const ulong K = 1024;
            static const ulong M = K*K;
            
            static const ulong P = 173;/* primorial base */
            static const ulong S = 2*M; /* segment size in bytes */
            static const uint64_t N = 3192*M; /* static prime limit */
            static const ulong Z = 128*M; /* sieve size limit */
            static constexpr double R = 20.0; /* limit prime ratio */
            static constexpr double H = 0.9; /* target heap usage */

* No dev fees (not sure how to send something from immature blocks..) but feel 
free to donate: RIC: RKkoa2hyouQKAL1F4YTLjmZ1HoLqSN6bBE

Usage
-----

        cd riecoin/src
        make -f makefile.unix
        ./riecoind -daemon
        sleep 40 && ./riecoind setgenerate true <#threads> <#sieveSize> &
        tail -f ~/.riecoin/debug.log

* Specify sieveSize by mHash using multiples of 16, otherwise the value 
is probed automatically.
